package controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Cidade;
import repository.CidadeRepository;

@RestController
@RequestMapping("/cidade")
public class CidadeController {
	
	
	private CidadeRepository repository;
	
	public CidadeController(CidadeRepository cidadeRepository) {
		
		this.repository = cidadeRepository;
		
	}
	@GetMapping(path = {"/{id}"})
	public ResponseEntity<Cidade> findById(@PathVariable long id){
	  return repository.findById(id)
	          .map(record -> ResponseEntity.ok().body(record))
	          .orElse(ResponseEntity.notFound().build());
	}
	@PostMapping
	public Cidade create(@RequestBody Cidade contact){
	    return repository.save(contact);
	}
	
	@PutMapping(value="/{id}")
	  public ResponseEntity<Cidade> update(@PathVariable("id") long id,
	                                        @RequestBody Cidade cidade){
	    return repository.findById(id)
	        .map(record -> {
	            record.setNome(cidade.getNome());
	            record.setEstado(cidade.getEstado());
	            Cidade updated = repository.save(record);
	            return ResponseEntity.ok().body(updated);
	        }).orElse(ResponseEntity.notFound().build());
	  }
	
	@GetMapping(value = "/pesquisaCidade/{name}")
	public ResponseEntity<List<Cidade>> findByNome(@PathVariable String name){

			List<Cidade> cidade = new ArrayList<Cidade>();
			cidade = repository.findByNome(name);
			
			return new ResponseEntity<List<Cidade>>(cidade,HttpStatus.OK);
		}
	
	@GetMapping(value = "/pesquisaEstado/{estado}")
	public ResponseEntity<List<Cidade>> findByEstado(@PathVariable String estado){

			List<Cidade> cidade = new ArrayList<Cidade>();
			cidade = repository.findByEstado(estado);
			
			return new ResponseEntity<List<Cidade>>(cidade,HttpStatus.OK);
		}

}
