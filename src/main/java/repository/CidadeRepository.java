package repository;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import model.Cidade;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Long>{

	
	public List<Cidade> findByNome(String nome);
	
	public List<Cidade> findByEstado(String estado);
	
}
