package com.projetocompasso.projetocompasso;

import java.util.stream.LongStream;

import javax.sql.DataSource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import model.Cidade;
import repository.CidadeRepository;



@SpringBootApplication
@EntityScan("model")
@EnableJpaRepositories("repository")
@ComponentScan(basePackages={"controller"})
public class ProjetoCompassoApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ProjetoCompassoApplication.class, args);
	}
	

//	@Bean
//	CommandLineRunner init(CidadeRepository repository) {
//		return args -> {
//			repository.deleteAll();
//			LongStream.range(1, 11)
//					.mapToObj(i -> {
//						Cidade c = new Cidade();
//						c.setNome("Cidade " + i);
//						c.setEstado("Estado" + i);
//						return c;
//					})
//					.map(v -> repository.save(v))
//					.forEach(System.out::println);
//		};
//	}
}
